import React from 'react'
import { Link } from 'react-router-dom';

const Login = () => {
  return (
    <form className='col-md-4 offset-md-4 border rounded p-4 mt-3 shadow'>
    <h2 className='text-center m-4'>Logga in</h2>
        <div class="mb-3">
            <label htmlFor='Email' className='form-label'>Email address</label>
            <input type={"text"} className="form-control" placeholder='Skriv email address' name='email'/>
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Lösenord</label>
            <input type={"password"} class="form-control" placeholder='Skriv ditt lösenord' name='password'/>
        </div>
        <button type="submit" class="btn btn-primary">Logga in</button>
        <br/>
        <Link className='text-primary stretched-link' to="/forgotpassword">Glömt Lösenord</Link>
        <Link className='text-primary stretched-link p-2' to="/adduser">Registrera Användare</Link>
    </form>
  );
};

export default Login;