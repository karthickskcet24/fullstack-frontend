import React from 'react'

const ForgotPassword = () => {
  return (
    <form className='col-md-4 offset-md-4 border rounded p-4 mt-3 shadow'>
    <h2 className='text-center m-4'>Glömt lösenordet</h2>
        <div class="mb-3">
            <label htmlFor='Email' className='form-label'>Email address</label>
            <input type={"text"} className="form-control" placeholder='Skriv email address' name='email'/>
        </div>
        <button type="submit" class="btn btn-primary">Skicka</button>
        <br/>
       
    </form>
  )
}

export default ForgotPassword
