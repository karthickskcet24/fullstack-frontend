import React, { useState } from 'react'
import axios from "axios";
import {Link, useNavigate} from "react-router-dom";

const AddUser = () => {

    let navigate = useNavigate();

    const [user,setUser]=useState({
        name:"",
        username:"",
        email:""
    })

    const {name,username,email} = user

    const onInputChange = (e)=>{
        setUser({...user, [e.target.name]: e.target.value});
    }

    const onSubmit = async (e) =>{
        e.preventDefault();
        await axios.post("http://localhost:8080/user", user)
        navigate("/")
    }

  return (
    <div className='container'>
        <div className='row'>
            <div className='col-md-6 offset-md-3 border rounded p-4 mt-2 shadow'>
                <h2 className='text-center m-4'>Registrera Användare</h2>
                <form onSubmit={(e) => onSubmit(e)}>
                <div className='mb-3'>
                    <label htmlFor='Name' className='form-label'>
                    Namn
                    </label>
                    <input type={"text"} className="form-control" placeholder='Skriv ditt namn' name='name' value={name} onChange={(e)=>onInputChange(e)}/>
                </div>
                <div className='mb-3'>
                    <label htmlFor='Username' className='form-label'>
                    AnvändarNamn
                    </label>
                    <input type={"text"} className="form-control" placeholder='Skriv ditt användarnamn' name='username' value={username} onChange={(e)=>onInputChange(e)}/>
                </div>
                <div className='mb-3'>
                    <label htmlFor='Email' className='form-label'>
                    Email
                    </label>
                    <input type={"text"} className="form-control" placeholder='Skriv email address' name='email' value={email} onChange={(e)=>onInputChange(e)}/>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Lösenord</label>
                    <input type={"password"} class="form-control" placeholder='Skriv ditt lösenord' name='password'/>
                </div>
                <button type='submit' className='btn btn-outline-primary'>Submit</button>
                <Link className='btn btn-outline-danger mx-2' to='/'>Cancel</Link>
                </form>
            </div>
        </div>
    </div>
  )
}

export default AddUser;